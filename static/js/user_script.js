/*global $*/

var listController = (function (){
    /*This counter is serving as a placeholder to assign IDs to wishlist items until I can setup the AJAX
    * calls to retrieve IDs from the database. However, I may not want to change this and instead keep this
    * as the internalID. That way, it will allow the UI to communicate with the data model about the items
    * displayed but not allow a hacker to change the HTML, resubmit and request any item in the database
    * based on the ID. */
    var id_counter = 1;

    var ListItem = function (obj) {

        // todo create an AJAX call to get a new id
        this.externalID = -1;
        this.internalID = -1;
        this.description = obj.description;
        this.price = obj.price;
        this.rank = obj.rank;
        this.category = obj.category;
        this.comments = obj.comments;
        this.link = obj.link;
    };

    var wishlist =  {
        allItems: []
    };

    ListItem.prototype.createID = function () {
        this.externalID = id_counter;
        id_counter += 1;
    };

    var findItembyID = function (id) {

        for (var i=0; i < wishlist.allItems.length; i++) {
            if (wishlist.allItems[i].externalID == id) {
                return wishlist.allItems[i];
            }
        }

        return false;
    };

    var trimURL = function (URL) {
        var newURL;

        if (URL.startsWith('https://') || URL.startsWith('http://')) {
            newURL = URL.split('//');
            newURL = newURL[1];
        }
        else {
            newURL = URL;
        }

        return newURL;
    };

    return {

        appVariables: {
            itemIDToDelete: -1
        },

        addListItem: function (obj) {
            newListItem = new ListItem(obj);

            newListItem.createID();

            newListItem.link = trimURL(newListItem.link);

            // add the new list item to the data model list using rank
            if (newListItem.rank === 'top') {
                wishlist.allItems.unshift(newListItem);

                // reassign the ranks of the items
                wishlist.allItems.forEach(function (element, index) {
                    element.rank = index;
                });
            }
            else {
                wishlist.allItems.push(newListItem);
                newListItem.rank = wishlist.allItems.length - 1;
            }

            return newListItem;
        },

        getWishlistItems: function () {
            return wishlist.allItems;
        },

        getItem: function (id) {

            for (var i=0; i < wishlist.allItems.length; i++) {
                if (wishlist.allItems[i].externalID == id) {
                    return wishlist.allItems[i];
                }
            }

            return false;
        },

        updateItem: function (obj) {

            var item = findItembyID(obj.externalID);

            if (item === false) {
                console.log('Item not found in list controller > updatedItem');
                return false;
            }
            else {
                item.description = obj.description;
                item.link = trimURL(obj.link);
                item.price = obj.price;
                item.category = obj.category;
                item.comments = obj.comments;
            }
        },

        removeItem: function (itemID) {

            // find the index of the item in question
            for (var i=0; i < wishlist.allItems.length; i++) {
                if (wishlist.allItems[i].externalID == itemID) {
                    // remove that item from the array
                    wishlist.allItems.splice(i, 1);

                     // update the rank of all objects in the array
                    wishlist.allItems.forEach(function (element, index) {
                        element.rank = index;
                    });

                    // item successfuly deleted
                    return true;
                }
            }

            // item failed to delete
            return false;
        }
    }



})();

var UIController = (function (){
    
    // setup DOM strings using Jquery
   var DOMStrings = {
       $addItemBtn: $('.add-item'),
       $rankButton: $('.rankbtn'),
       $addCancelBtn: $('.add-item-cancel'),
       $addSaveBtn: $('.add-item-save'),
       $addModal: $('.add-modal'),
       $accountBtn: $('.account'),
       $wishlist: $('.list'),
       $editBtn: $('.edit'),
       $editModal: $('.edit-modal'),
       $editCancelBtn: $('.edit-item-cancel'),
       $editSaveBtn: $('.edit-item-save'),
       $removeModal: $('.remove-modal'),
       $removeYesBtn: $('.remove-yes'),
       $removeNoBtn: $('.remove-no')
   };

   // add an item to the wish list
    var drawItem = function (item) {

        var $newItem = $('\t\t\t<div class="list-card" id="' + item.externalID +'">\n' +
            '                <div class="title-bar">\n' +
            '                    <div class="item-name">' + item.description + '</div>\n' +
            '                    <div class="item-price">' + item.price + '</div>\n' +
            '                </div>\n' +
            '                <div class="item-category">' + item.category + '</div>\n' +
            '                <div class="item-details">\n' +
            '                    <div class="item-image">\n' +
            '                        <img src="img/placeholder-item-image.png">\n' +
            '                    </div> \n' +
            '                    <div class="item-description">\n' +
            '                        ' + item.comments + '\n' +
            '                    </div>\n' +
            '                    <div class="item-buttons">\n' +
            '                        <button class="view-in-store">View in Store</button>\n' +
            '                        <button class="edit">Edit</button>\n' +
            '                        <button class="remove">Remove</button>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '        </div>');

        $('.list').append($newItem);

    };

    return {
        
        getDOMStrings: function() {
            return DOMStrings;
        },

        closeAddModal: function() {
            document.getElementById('addForm').reset();

            // reset the rank buttons
            $('.add-to-bottom').removeClass('selected');
            $('.add-to-bottom').addClass('deselected');
            $('.add-to-top').addClass('selected');
            $('.add-to-top').removeClass('deselected');

            // close the modal
            DOMStrings.$addModal.hide();
        },

        closeEditModal: function () {
            document.getElementById('editForm').reset();

            DOMStrings.$editModal.hide();
        },

        // draw the entire wishlist
        drawWishlist: function (list) {
            // remove the current wishlist
            DOMStrings.$wishlist.empty();

            list.forEach(function (item) {
                drawItem(item);
            });
        }
    }
    
})();

var masterController = (function(listCtrl, UICtrl){

    var DOM = UICtrl.getDOMStrings();

    var setupEventListeners = function () {

        // navbar listeners
        DOM.$addItemBtn.on('click', function(){DOM.$addModal.show();});
        // DOM.$accountBtn.on('click', UICtrl.drawWishlist);

        // add modal listeners
        DOM.$rankButton.on('click', changeRank);
        DOM.$addSaveBtn.on('click', addItemToList);
        DOM.$addCancelBtn.on('click', UICtrl.closeAddModal);

        // edit modal listeners
        DOM.$editSaveBtn.on('click', saveEditModal);
        DOM.$editCancelBtn.on('click', UICtrl.closeEditModal);

        // remove modal listeners
        DOM.$removeYesBtn.on('click', removeItem);
        DOM.$removeNoBtn.on('click', closeRemoveModal);

        // wishlist listeners requiring event delegation
        DOM.$wishlist.on('click', '.edit', showEditModal);
        DOM.$wishlist.on('click', '.view-in-store', showInStore);
        DOM.$wishlist.on('click', '.remove', showRemoveModal);

    };
    
    // change state of the buttons used for choosing rank of the new item
    var changeRank = function () {
        
        // only run when the button in question is deselected
        if ($(this).hasClass('deselected')) {
            
            // figure out which button has been clicked and change the state
            if ($(this).hasClass('add-to-top')) {
                    $(this).removeClass('deselected');
                    $(this).addClass('selected');
                    $('.add-to-bottom').removeClass('selected');
                    $('.add-to-bottom').addClass('deselected');
                }
            else {
                    $(this).removeClass('deselected');
                    $(this).addClass('selected');
                    $('.add-to-top').removeClass('selected'); 
                    $('.add-to-top').addClass('deselected');
                }
        }
    };

    var addItemToList = function () {
        
        // initialize variables for needed data
        var formInput = {
            
            // collect new list item data
            description: $('.add-modal .description-input').val(),
            link: $('.add-modal .link-input').val(),
            price: $('.add-modal .price-input').val(),
            category: $('.add-modal #category-select').val(),
            rank: $('.add-modal .selected').attr('id'),
            comments: $('.add-modal .comment-input').val()
        };

        // add the user's wishlist item to the data model
        listCtrl.addListItem(formInput);

        // send the data model to the UI to update it create a draw wishlist feature
        UICtrl.drawWishlist(listCtrl.getWishlistItems());

        UICtrl.closeAddModal();
    };

    var showEditModal = function () {
        // get the item ID
        itemID = $(this).parents().filter('.list-card').attr('id');

        // get the item object from the datamodel
        var item = listCtrl.getItem(itemID);

        if (item === false) {
            console.log('Could not find item');
        }

        // change the content in the edit modal to show the content of the clicked card
        $('.edit-modal-container-titlebar span').text('Edit ' + item.description);
        $('.edit-item-form .description-input').val(item.description);
        $('.edit-item-form .link-input').val(item.link);
        $('.edit-item-form .price-input').val(item.price);
        $('.edit-item-form #category-select').val(item.category);
        $('.edit-item-form .comment-input').val(item.comments);

        $('.edit-modal').attr('id', itemID);

        // display the edit modal
        DOM.$editModal.show();
    };

    var saveEditModal = function () {
        // collect all the changes in the edit modal
        var formInput = {

            // collect new list item data
            externalID: $('.edit-modal').attr('id'),
            description: $('.edit-modal .description-input').val(),
            link: $('.edit-modal .link-input').val(),
            price: $('.edit-modal .price-input').val(),
            category: $('.edit-modal #category-select').val(),
            comments: $('.edit-modal .comment-input').val()
        };

        // add the changes to the wishlist item to the data model
        listCtrl.updateItem(formInput);

        // send the data model to the UI to update it create a draw wishlist feature
        UICtrl.drawWishlist(listCtrl.getWishlistItems());

        UICtrl.closeEditModal();

    };

    var showInStore = function () {
        // get the item ID
        itemID = $(this).parents().filter('.list-card').attr('id');

        // get the item object from the datamodel
        var item = listCtrl.getItem(itemID);

        var win = window.open('http://' + item.link, '_blank');

        if (win) {
            //Browser has allowed it to be opened
            win.focus();
        } else {
            //Browser has blocked it
            alert('Please allow popups for this website');}
    };

    var showRemoveModal = function() {
        // show modal
        DOM.$removeModal.show();

        // updateID
        listCtrl.appVariables.itemIDToDelete = $(this).parents().filter('.list-card').attr('id');
    };

    var closeRemoveModal = function () {
        listCtrl.appVariables.itemIDToDelete = -1;
        DOM.$removeModal.hide();
    };

    var removeItem = function () {
        // remove the item
        var itemRemoved = listCtrl.removeItem(listCtrl.appVariables.itemIDToDelete);

        if (!itemRemoved) {
            alert('Failed to remove item.')
        }

        // redraw the list
        UICtrl.drawWishlist(listCtrl.getWishlistItems());

        closeRemoveModal();

    };

    return {
        init: function () {
            console.log('Application has started.');
            setupEventListeners();
        }
    };

})(listController, UIController); 

masterController.init();