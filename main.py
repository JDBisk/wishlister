import os
from flask import Flask, jsonify, render_template, request, url_for, session, redirect, flash, send_from_directory
from flask_jsglue import JSGlue
from flask_session import Session
from flask_sqlalchemy import SQLAlchemy

# application config
app.config['DEBUG'] = True

# application variables
app = Flask(__name__)
db = SQLAlchemy()

@app.route('/')
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run()